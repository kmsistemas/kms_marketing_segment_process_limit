# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This module,
#    Copyright (C) 2012 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name" : "kms_marketing_segment_process_limit",
    "version" : "1.0",
    "description" : """\
This module allows to set a limit the number of emails processed per time unit
in a marketing segment. Time unit is defined by the planned action that
processes marketing segments.

""",
    "author" : "KM Sistemas de información, S.L.",
    "website" : "http://www.kmsistemas.com",
    "depends" : [
    	'marketing_campaign',
    ],
    "category" : "Added functionality",
    "init_xml" : [],
    "demo_xml" : [],
    "update_xml" : [
        'marketing_segment_view.xml',
    ],
    "active": False,
    "installable": True
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
