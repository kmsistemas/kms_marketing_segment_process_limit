# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved
#
#    This module,
#    Copyright (C) 2012 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

import time
from osv import osv, fields
# from datetime import datetime

class marketing_campaign_segment(osv.osv):

    _inherit = 'marketing.campaign.segment'

    def _check_nonnegative_number(self, cr, uid, ids):
        """
        Check if number is negative.
        """
        for line in self.browse(cr, uid, ids):
            if line and (line.power < 0):
                return False
        return True

    _columns = {
        'mails_limit': fields.integer('Mails limit', help = "Maximum number of mails processed per time unit for this segment. Time unit is defined on the planned action that syncs marketing segments."),
    }

    _defaults = {
        'mails_limit': lambda *a: 50,
    }

    _constraints = [(_check_nonnegative_number, 'Error: this number cannot be negative', ['mails_limit']), ]



    def process_segment(self, cr, uid, segment_ids = None, context = None):
        Workitems = self.pool.get('marketing.campaign.workitem')
        if not segment_ids:
            segment_ids = self.search(cr, uid, [('state', '=', 'running')], context = context)

        action_date = time.strftime('%Y-%m-%d %H:%M:%S')
        campaigns = set()
        for segment in self.browse(cr, uid, segment_ids, context = context):
            if segment.campaign_id.state != 'running':
                continue

            campaigns.add(segment.campaign_id.id)
            act_ids = self.pool.get('marketing.campaign.activity').search(cr,
                  uid, [('start', '=', True), ('campaign_id', '=', segment.campaign_id.id)], context = context)

            model_obj = self.pool.get(segment.object_id.model)
            criteria = []
            if segment.sync_last_date and segment.sync_mode != 'all':
                criteria += [(segment.sync_mode, '>', segment.sync_last_date)]
            if segment.ir_filter_id:
                criteria += eval(segment.ir_filter_id.domain)
            object_ids = model_obj.search(cr, uid, criteria, context = context)

            # XXX TODO: rewrite this loop more efficiently without doing 1 search per record!
            kms_counter = 0
            for o_ids in model_obj.browse(cr, uid, object_ids, context = context):
                if kms_counter < segment.mails_limit or segment.mails_limit == 0:
                    # avoid duplicated workitem for the same resource
                    if segment.sync_mode in ('write_date', 'all'):
                        wi_ids = Workitems.search(cr, uid, [('res_id', '=', o_ids.id), ('segment_id', '=', segment.id)], context = context)
                        if wi_ids:
                            continue

                    wi_vals = {
                        'segment_id': segment.id,
                        'date': action_date,
                        'state': 'todo',
                        'res_id': o_ids.id
                    }

                    partner = self.pool.get('marketing.campaign')._get_partner_for(segment.campaign_id, o_ids)
                    if partner:
                        wi_vals['partner_id'] = partner.id

                    for act_id in act_ids:
                        wi_vals['activity_id'] = act_id
                        Workitems.create(cr, uid, wi_vals, context = context)
                    kms_counter += 1

            self.write(cr, uid, segment.id, {'sync_last_date':action_date}, context = context)
        Workitems.process_all(cr, uid, list(campaigns), context = context)
        return True


# KMS CUSTOM VERSION AS OF JAN 2013
#    def process_segment(self, cr, uid, segment_ids=None, context=None):
#
#
#        currdate = datetime.now()
#        # Solamente queremos enviar correos de lunes a jueves de 8 a 20 h, y los viernes de 8 a 12 h. (sin tener en cuenta minutos)
#        if (0 <= currdate.weekday() <=3 and 8 <= currdate.hour <= 20) or (currdate.weekday() == 4 and 8 <= currdate.hour <= 12):
#
#            Workitems = self.pool.get('marketing.campaign.workitem')
#            if not segment_ids:
#                segment_ids = self.search(cr, uid, [('state', '=', 'running')], context=context)
#
#            action_date = time.strftime('%Y-%m-%d %H:%M:%S')
#            campaigns = set()
#            for segment in self.browse(cr, uid, segment_ids, context=context):
#                if segment.campaign_id.state != 'running':
#                    continue
#
#                campaigns.add(segment.campaign_id.id)
#                act_ids = self.pool.get('marketing.campaign.activity').search(cr,
#                      uid, [('start', '=', True), ('campaign_id', '=', segment.campaign_id.id)], context=context)
#
#                model_obj = self.pool.get(segment.object_id.model)
#                criteria = []
#                if segment.sync_last_date and segment.sync_mode != 'all':
#                    criteria += [(segment.sync_mode, '>', segment.sync_last_date)]
#                if segment.ir_filter_id:
#                    criteria += eval(segment.ir_filter_id.domain)
#                object_ids = model_obj.search(cr, uid, criteria, context=context)
#
#                # XXX TODO: rewrite this loop more efficiently without doing 1 search per record!
#                kms_counter = 0
#                for o_ids in model_obj.browse(cr, uid, object_ids, context=context):
#                    # KMS: We hardcode the limit to 50 items per Segment Sync cycle
#                    if kms_counter < segment.mails_limit or segment.mails_limit == 0:
#                        # avoid duplicated workitem for the same resource
#                        if segment.sync_mode in ('write_date','all'):
#                            wi_ids = Workitems.search(cr, uid, [('res_id','=',o_ids.id),('segment_id','=',segment.id)], context=context)
#                            if wi_ids:
#                                continue
#
#                        wi_vals = {
#                            'segment_id': segment.id,
#                            'date': action_date,
#                            'state': 'todo',
#                            'res_id': o_ids.id
#                        }
#
#                        partner = self.pool.get('marketing.campaign')._get_partner_for(segment.campaign_id, o_ids)
#                        if partner:
#                            wi_vals['partner_id'] = partner.id
#
#                        for act_id in act_ids:
#                            wi_vals['activity_id'] = act_id
#                            Workitems.create(cr, uid, wi_vals, context=context)
#                        kms_counter += 1
#
#                self.write(cr, uid, segment.id, {'sync_last_date':action_date}, context=context)
#            Workitems.process_all(cr, uid, list(campaigns), context=context)
#
#        return True

marketing_campaign_segment()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
